let inputNumber1 = prompt("Enter the first number you want to calculate:");
let inputNumber2 = prompt("Enter the second number you want to calculate:");

while (inputNumber1 === null || inputNumber1.trim() === "" || isNaN(+inputNumber1) || inputNumber1 % 1 !== 0) {
    inputNumber1 = prompt("Enter the first CORRECT number you want to calculate:", inputNumber1);
}
while (inputNumber2 === null || inputNumber2.trim() === "" || isNaN(+inputNumber2) || inputNumber2 % 1 !== 0) {
    inputNumber2 = prompt("Enter the second CORRECT number you want to calculate:", inputNumber2);
}

let inputOperation = prompt("Enter the operation sign (+, -, *, /):");

console.log(calculation(inputNumber1, inputNumber2, inputOperation));

function calculation(inputNumber1, inputNumber2, inputOperation) {

    switch (inputOperation) {
        case "+":
            return +inputNumber1 + +inputNumber2;
        case "-":
            return inputNumber1 - inputNumber2;
        case "*":
            return inputNumber1 * inputNumber2;
        case "/": {
            if (+inputNumber2 === 0) {
                return "Can not be divided by 0";
            } else {
                return inputNumber1 / inputNumber2;
            }
        }
    }
}



