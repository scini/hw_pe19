const bodyStyle = document.querySelector('.top_screen');
const headerDiv = document.querySelector('.header');
const logo = document.querySelector('.green_link');
const headerLinks = document.querySelectorAll('.header_right_link');
const greenBlock = document.querySelector('.green_block');
const loginLink = document.querySelector('.login');
const submitBtn = document.querySelector('.submit');

const themeBtn = document.createElement('input');
themeBtn.type = 'button';
themeBtn.value = 'Сменить тему';
themeBtn.classList.add('change_theme_btn');
headerDiv.append(themeBtn);

let click_minder = false;

window.onload = () => (localStorage.getItem('theme') !== null) ? redTheme() : greenTheme();

let themeChanger = () => click_minder ? redTheme() : greenTheme();

themeBtn.addEventListener('click', themeChanger);

function redTheme() {
    bodyStyle.classList.add('theme');
    logo.classList.add('theme');
    headerLinks.forEach(el => el.classList.add('theme'));
    greenBlock.classList.add('theme');
    loginLink.classList.add('theme');
    submitBtn.classList.add('theme');
    click_minder = false;
    localStorage.setItem('theme', 'red');
}

function greenTheme() {
    bodyStyle.classList.remove('theme');
    logo.classList.remove('theme');
    headerLinks.forEach(el => el.classList.remove('theme'));
    greenBlock.classList.remove('theme');
    loginLink.classList.remove('theme');
    submitBtn.classList.remove('theme');
    localStorage.clear();
    click_minder = true;
}


