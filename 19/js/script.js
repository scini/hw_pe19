const imageWrapper = document.querySelector('.images-wrapper');
const images = document.querySelectorAll('.image-to-show');

const divButton = document.createElement('div');
divButton.classList.add('button_container');
imageWrapper.append(divButton);

const pauseButton = document.createElement('input');
pauseButton.type = 'button';
pauseButton.value = 'Прекратить';
pauseButton.classList.add('pause_btn');
divButton.append(pauseButton);

const returnButton = document.createElement('input');
returnButton.type = 'button';
returnButton.value = 'Возобновить показ';
returnButton.classList.add('return_btn');
divButton.append(returnButton);

const timer = document.createElement('p');
timer.classList.add('timer');
imageWrapper.append(timer);

let counter = '';
let imageChanger = '';

showImage();
imgChanger();
countDown();

pauseButton.addEventListener('click', () => {
    clearInterval(imageChanger);
    pauseButton.classList.add('active');
    returnButton.classList.add('active');
});

returnButton.addEventListener('click', () => {
    imgChanger();
    countDown();
    returnButton.classList.remove('active');
});

function imgChanger() {
    imageChanger = setInterval(showImage, 10000);
}

function showImage() {
    if (counter >= images.length - 1) {
        images.forEach(el => el.classList.remove('active'));
        counter = 0;
        images[counter].classList.add('active');
    } else {
        images.forEach(el => el.classList.remove('active'));
        counter++;
        images[counter].classList.add('active');
    }
    countDown();
}

function countDown() {
    let startTime = Date.now();
    let timeInterval = setInterval(function () {
        let elapsedTime = Date.now() - startTime;
        let countDown = ((10000 - elapsedTime) / 1000).toFixed(3);
        let seconds = countDown.slice(0, 1);
        let ms = countDown.slice(2, 5);
        (countDown > 0) ? timer.innerHTML = seconds + ' сек ' + ms + ' мс' : clearInterval(timeInterval);
        pauseButton.addEventListener('click', () => {
            clearInterval(timeInterval);
            timer.innerHTML = '';
        });
    });
}




