let filterBy = function(userData, userDataType) {
    return userData.filter(el => typeof el !== userDataType);
};

console.log(filterBy(['hello', 'world', 23, '23', null], 'number'));

