const button = document.querySelectorAll('.btn');
const keyEnter = document.querySelector('.keyEnter');
const keyS = document.querySelector('.keyS');
const keyE = document.querySelector('.keyE');
const keyO = document.querySelector('.keyO');
const keyN = document.querySelector('.keyN');
const keyL = document.querySelector('.keyL');
const keyZ = document.querySelector('.keyZ');

button.forEach(el => el.addEventListener('keypress', buttonLighter));

function colorEraser(){
    button.forEach(el => el.classList.remove('active'))
}
function buttonLighter(event) {
    const pressedButton = event.code;
    switch (pressedButton) {
        case 'Enter':
            colorEraser();
            keyEnter.classList.add('active');
            break;
        case 'KeyS':
            colorEraser();
            keyS.classList.add('active');
            break;
        case 'KeyE':
            colorEraser();
            keyE.classList.add('active');
            break;
        case 'KeyO':
            colorEraser();
            keyO.classList.add('active');
            break;
        case 'KeyN':
            colorEraser();
            keyN.classList.add('active');
            break;
        case 'KeyL':
            colorEraser();
            keyL.classList.add('active');
            break;
        case 'KeyZ':
            colorEraser();
            keyZ.classList.add('active');
            break;
    }
}

