const tabs = document.querySelectorAll('.tabs li');
tabs.forEach((element) => {
    if(!element.classList.contains('tabs-title')) {
        element.classList.add('tabs-title');
    }
});

const tabsContent = document.querySelectorAll('.tabs-content li');
tabsContent.forEach((element) =>
    element.classList.add('tabs-text'));
tabsContent[0].classList.add('active');

tabs.forEach((element, i) =>
    element.addEventListener('click', function () {
        for (let j = 0; j < tabs.length; j++) {
            tabs[j].classList.remove('active');
            tabsContent[j].classList.remove('active');
        }
        tabs[i].classList.add('active');
        tabsContent[i].classList.add('active');
    }));
