let body = document.querySelector('.body');
let table = document.createElement('table');
table.className = 'table_style';
body.prepend(table);
let tableRow = '';
let tableColumn = '';

for (let i = 0; i < 30; i++) {
    creatingRow();
}

function creatingRow() {
    tableRow = document.createElement('tr');
    tableRow.className = 'table_row_style';
    table.prepend(tableRow);
    for (let i = 0; i < 30; i++) {
        creatingColumn();
    }
}

function creatingColumn() {
    tableColumn = document.createElement('td');
    tableColumn.className = 'table_column_style';
    tableRow.append(tableColumn);
}

table.addEventListener('click', changeColor);

function changeColor(event) {
    let tdTarget = event.target;
    tdTarget.classList.toggle('active');
}

body.addEventListener('click', bodyClick);

function bodyClick(event) {
    let tableTarget = event.target;
    if (tableTarget === body) {
        table.classList.toggle('active');
    }
}

