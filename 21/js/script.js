$(function () {

    $('.menu_js a').on('click', function () {
        let idName = $(this).attr('href');
        $('html,body').animate({scrollTop: $(idName).offset().top}, 1000);
    });


    $(window).scroll(function () {
        $(this).scrollTop() > window.innerHeight ? $('.up_btn').fadeIn() : $('.up_btn').fadeOut();
    });

    $('.up_btn').on('click', function () {
        $('html, body').animate({scrollTop: 0}, 1000);
    });

    $('.hide_btn').on('click', function (){
        $('.top_rated').slideToggle();
    })

});