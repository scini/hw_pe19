const numbersBtn = document.querySelectorAll('.numbers');
const signsBtn = document.querySelectorAll('.sign');
const display = document.querySelector('.calc_display');
const dotBtn = document.querySelector('.dot');
const clearBtn = document.querySelector('.clear');
const memoryClearBtn = document.querySelector('.memory_data_clear');
const memoryPlusBtn = document.querySelector('.memory_plus');
const memoryMinusBtn = document.querySelector('.memory_minus');
const displayDiv = document.querySelector('.display');
const spanMemory = document.createElement('span');
spanMemory.className = 'memory_screen_btn';

let currentEnteredNumber = '';
let enteredSign = '';
let numberSwitcher = false;
let toClear = '';
let temporaryMemory = '';
let temporaryDotMemory = '';
let memoryPlus = '';
let memoryCleanerSwitch = false;
let inputKeyboard = '';

numbersBtn.forEach((el) => el.addEventListener('click', (event) => enterNumber(event.target.value)));
signsBtn.forEach((el) => el.addEventListener('click', (event) => operationBlock(event.target.value)));
dotBtn.addEventListener('click', (event) => dotAdder(event.target.value));
clearBtn.addEventListener('click', (event) => clearDisplay(event.target.value));
memoryPlusBtn.addEventListener('click', (event) => memoryPlusKeeper(event.target.value));
memoryMinusBtn.addEventListener('click', (event) => memoryMinusKeeper(event.target.value));
memoryClearBtn.addEventListener('click', (event) => memoryCleaner(event.target.value));
display.addEventListener('keydown', keyboardNumber);

function keyboardNumber(event) {
    inputKeyboard = event.key;
    if (inputKeyboard >= 0 && inputKeyboard <= 9) {
        return enterNumber(inputKeyboard);
    }
    switch (inputKeyboard) {
        case '+':
            operationBlock(inputKeyboard);
            break;
        case '-':
            operationBlock(inputKeyboard);
            break;
        case '*':
            operationBlock(inputKeyboard);
            break;
        case '/':
            operationBlock(inputKeyboard);
            break;
        case 'Enter':
            operationBlock('=');
            break;
        case '.':
            dotAdder(inputKeyboard);
            break;
        case 'Delete':
            clearDisplay('C');
            break;
        case 'Backspace':
            clearDisplay('C');
            break;
    }
}

function enterNumber(enteredNumber) {
    if (numberSwitcher) {
        display.value = enteredNumber;
        numberSwitcher = false;
    } else {
        if (display.value === '0') {
            display.value = enteredNumber;
        } else {
            display.value += enteredNumber;
        }
    }
    memoryCleanerSwitch = false;
}

function operationBlock(operationSign) {
    temporaryMemory = display.value;
    if (numberSwitcher && enteredSign !== '=') {
        display.value = currentEnteredNumber;
    } else {
        numberSwitcher = true;
        if (enteredSign === '+') {
            currentEnteredNumber += +temporaryMemory;
        } else if (enteredSign === '-') {
            currentEnteredNumber -= +temporaryMemory;
        } else if (enteredSign === '*') {
            currentEnteredNumber *= +temporaryMemory;
        } else if (enteredSign === '/') {
            currentEnteredNumber /= +temporaryMemory;
        } else {
            currentEnteredNumber = +temporaryMemory;
        }
        display.value = currentEnteredNumber;
        enteredSign = operationSign;
        memoryCleanerSwitch = false;
    }
}

function clearDisplay(clearScreen) {
    toClear = clearScreen;
    if (toClear === 'C') {
        display.value = '0';
        currentEnteredNumber = +'';
        temporaryMemory = +'';
        numberSwitcher = false;
        memoryCleanerSwitch = false;
    }
}

function dotAdder() {
    temporaryDotMemory = display.value;
    if (numberSwitcher) {
        temporaryDotMemory = '0.';
        numberSwitcher = false;
    } else {
        if (temporaryDotMemory.indexOf('.') === -1) {
            temporaryDotMemory += '.';
        }
    }
    display.value = temporaryDotMemory;
    memoryCleanerSwitch = false;
}

function memoryPlusKeeper() {
    memoryPlus = display.value;
    addDisplayMemorySign();
    memoryCleanerSwitch = false;
}

function memoryMinusKeeper() {
    memoryPlus = display.value * (-1);
    addDisplayMemorySign();
    memoryCleanerSwitch = false;
}

function addDisplayMemorySign() {
    spanMemory.innerHTML = 'm';
    spanMemory.classList.add('active');
    displayDiv.append(spanMemory);
}

function memoryCleaner() {
    if (memoryCleanerSwitch) {
        memoryPlus = +'';
        spanMemory.classList.remove('active');
    } else {
        numberSwitcher = false;
        display.value = memoryPlus;
        memoryCleanerSwitch = true;
    }
}


