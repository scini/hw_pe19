let user = createNewUser();

function createNewUser() {
    const newUser = {
        firstName: prompt("Enter your name:"),
        lastName: prompt("Enter your last name:"),
        birthday: prompt("Enter your date of birth (dd.mm.yyyy):"),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge() {
            let todayDate = new Date();
            let birthdayDate = new Date(`${this.birthday.slice(6)}.${this.birthday.slice(3, 5)}.${this.birthday.slice(0, 2)}`);
            return ((todayDate.getMonth() < birthdayDate.getMonth()) || (todayDate.getMonth() === birthdayDate.getMonth() && todayDate.getDate() < birthdayDate.getDate())) ? todayDate.getFullYear() - birthdayDate.getFullYear() -1 : todayDate.getFullYear() - birthdayDate.getFullYear();
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        }
    };
    return newUser;
}

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());



