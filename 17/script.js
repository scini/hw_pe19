const enterPassword = document.querySelector('.enter_password');
const confirmPassword = document.querySelector('.confirm_password');
const eye = document.querySelector('.fa-eye');
const eyeSlash = document.querySelector('.fa-eye-slash');
eyeSlash.className = 'fas fa-eye icon-password';
const confirmButton = document.querySelector('.btn');
const underText = document.createElement('span');
let enteredPassword = '';
let enteredConfirmPassword = '';

enterPassword.addEventListener('keydown', passwordGetter);

function passwordGetter() {
    enteredPassword = this.value;
}

eye.addEventListener('click', showPassword);

function showPassword() {
    enterPassword.type === 'text' ? enterPassword.type = 'password' : enterPassword.type = 'text';
    eye.classList.toggle('fa-eye-slash');
}

confirmPassword.addEventListener('keydown', passwordConfirmGetter);

function passwordConfirmGetter() {
    enteredConfirmPassword = this.value;
}

eyeSlash.addEventListener('click', showConfirmPassword);

function showConfirmPassword() {
    confirmPassword.type === 'text' ? confirmPassword.type = 'password' : confirmPassword.type = 'text';
    eyeSlash.classList.toggle('fa-eye-slash');
}

confirmButton.addEventListener('click', verifyingPasswords);

function verifyingPasswords(e) {
    e.preventDefault();
    if (enteredPassword === enteredConfirmPassword) {
        alert('You are welcome');
        [enterPassword, confirmPassword].forEach(el => el.value = "");
        [eye, eyeSlash].forEach(el => el.className = 'fas fa-eye icon-password');
        [enterPassword, confirmPassword].forEach(el => el.type = 'password');
    } else creatingSpan()
}

function creatingSpan() {
    underText.className = 'under_text';
    underText.innerText = 'Нужно ввести одинаковые значения';
    confirmPassword.after(underText);
}

[enterPassword, confirmPassword].forEach(el => el.addEventListener('focus', deleteUnderSpan));

function deleteUnderSpan() {
    underText.remove();
}