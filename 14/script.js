let listId = document.getElementById('listId');
let timerId = document.getElementById('timerId');
let initialArray = ["I", "am", "trying", "to", 'list', "this", "array"];
let timeOut = 10;

function listCreator(){
    let listIdT = document.createElement('ul');
    listId.append(listIdT);
    return listIdT;
}

function createListByArray(listIdT) {
    let listOfItems = "";
    initialArray.map(function (el) {
        return listOfItems += "<li>" + `${el}` + "</li>";
    });
    return listIdT.innerHTML = listOfItems;
}

function timer() {
    timerId.innerHTML = timeOut;
    if (timeOut === 0) {
        listId.remove();
        timerId.remove();
    }
    timeOut--;
    setTimeout(timer, 1000);
}

createListByArray(listCreator(), initialArray);
timer();


