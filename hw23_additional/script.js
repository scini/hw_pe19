const body = document.querySelector('body');

const divMainContainer = document.createElement('div');
divMainContainer.classList.add('main_container');
body.append(divMainContainer);

const previousButton = document.createElement('input');
previousButton.classList.add('button');
previousButton.type = 'button';
previousButton.value = '<< Previous';
divMainContainer.prepend(previousButton);

const nextButton = document.createElement('input');
nextButton.classList.add('button');
nextButton.type = 'button';
nextButton.value = 'Next >>';
divMainContainer.append(nextButton);

const divImageContainer = document.createElement('div');
divImageContainer.classList.add('image_container');
nextButton.before(divImageContainer);

const image = document.createElement('img');
image.classList.add('image');
image.src = 'images/1.png';
divImageContainer.append(image);

const imageArray = ["images/1.png", "images/2.png", "images/3.png", "images/4.png", "images/5.png", "images/6.png"];
let imageCounter = 0;

previousButton.addEventListener('click', previousImage);
nextButton.addEventListener('click', nextImage);

function previousImage() {
    if (imageCounter <= 0) {
        imageCounter = imageArray.length - 1;
        image.src = imageArray[imageCounter];
    } else {
    imageCounter--;
    image.src = imageArray[imageCounter];
    }
}

function nextImage() {
    if(imageCounter >= imageArray.length-1) {
        imageCounter = 0;
        image.src = imageArray[imageCounter]
    } else {
    imageCounter++;
    image.src = imageArray[imageCounter];
    }
}

