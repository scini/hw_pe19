$(function () {
    $('.tabs-title:first').addClass('active');
    $('ul.tabs-content li:first').addClass('active');

    $('ul.tabs').on('click', 'li:not(.active)', function () {
        $(this).addClass('active')
            .siblings().removeClass('active')
            .closest('div.centered-content').find('ul.tabs-content li').removeClass('active').eq($(this).index()).addClass('active');
    });
});

