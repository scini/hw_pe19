let inputPrice = document.querySelector('.input_price');

// Creating input
let inputForm = document.createElement('input');
inputForm.type = 'text';
inputForm.className = 'input_form';
inputForm.setAttribute('value', '');
inputPrice.append(inputForm);

// Creating text before input
let inputText = document.createElement('p');
inputText.innerText = 'Price, UAH: ';
inputText.className = 'input_text';
inputForm.before(inputText);

// Creating container for upper input text
let upDiv = document.createElement('div');
upDiv.className = 'up_div';
inputPrice.before(upDiv);

// Creating container for under input text
let outDiv = document.createElement('div');
outDiv.className = 'out_div';
inputPrice.after(outDiv);

// Creating span with incorrect value text
let incorrectValue = document.createElement('span');
incorrectValue.className = 'incorrect_value';

// Creating upper span with price
let currentPrice = document.createElement('span');
currentPrice.className = 'additional_text';

// Creating delete button
let removeButton = document.createElement('button');
removeButton.className = 'remove_button';

// Upper text and button remover
function removeR() {
    currentPrice.remove();
    removeButton.remove();
}

// Input events
inputForm.addEventListener('focus', onFocus);

function onFocus() {
    removeR();
    inputForm.classList.remove('incorrect');
    incorrectValue.innerText = '';
    outDiv.prepend(incorrectValue);
    inputForm.classList.add('active');
}

inputForm.addEventListener('blur', onBlur);

function onBlur() {
    if (inputForm.value < 0 || isNaN(+inputForm.value)) {
        removeR();
        inputForm.classList.add('incorrect');
        incorrectValue.innerText = 'Please enter correct price';
    } else if (inputForm.value.trim() === '') {
        inputText.value = '';
    } else {
        inputForm.classList.add('colored');
        currentPrice.innerText = 'Текущая цена: ' + `${inputForm.value}` + ' UAH';
        upDiv.append(currentPrice);
        removeButton.innerText = 'X';
        currentPrice.after(removeButton);
        removeButton.addEventListener('click', function () {
            removeR();
            inputForm.value = '';
        });
    }
}

